import numpy as np
import cv2
import math


def new_reflet(img, circles):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)
    _, im_bw = cv2.threshold(gray, 254, 255, cv2.THRESH_BINARY)

    # Extrayez les points les plus brillants de la zone et affichez leurs coordonnées dans deux listes de masse_x et masse_y
    mass_x, mass_y = np.where(im_bw >= 255)
    cent_x = mass_x[0]
    cent_y = mass_y[0]
    length_mass = len(mass_x)
    len_L = 10

    # Parcourez toute la liste des points les plus brillants, calculez la distance entre chaque point et le centre du cercle
    # et affichez les coordonnées du point le plus proche du centre du cercle comme coordonnées du reflet
    for i in range(length_mass):
        length = math.hypot(mass_x[i] - circles[0][1], mass_y[i] - circles[0][0])
        if length < len_L:
            len_L = length
            cent_x = mass_x[i]
            cent_y = mass_y[i]
    return cent_x, cent_y
