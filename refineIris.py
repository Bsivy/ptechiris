import numpy as np
import cv2
import os
import matplotlib.pyplot as plt


def refineIris(img, faceImage, name):
    image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    pixel_values = image.reshape((-1, 3))
    pixel_values = np.float32(pixel_values)
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.2)
    k = 5
    _, labels, (centers) = cv2.kmeans(pixel_values, k, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)
    centers = np.uint8(centers)
    labels = labels.flatten()
    segmented_image = centers[labels.flatten()]
    segmented_image = segmented_image.reshape(image.shape)
    cv2.imwrite("../Photo/Results/seg_res/" + name, segmented_image)
    gray = cv2.cvtColor(segmented_image, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)
    gray[np.where((img == [0]).all(axis=2))] = [255]
    _, im_bw = cv2.threshold(gray, 254, 255, cv2.THRESH_BINARY)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
    closing = cv2.morphologyEx(im_bw, cv2.MORPH_CLOSE, kernel)
    closing = cv2.bitwise_not(closing)

    # detect circles in the image
    circles = cv2.HoughCircles(closing, cv2.HOUGH_GRADIENT, 1, 1000, param1=15, param2=10, minRadius=1, maxRadius=50)

    # ensure at least some circles were found
    if circles is not None:
        # convert the (x, y) coordinates and radius of the circles to integers
        circles = np.round(circles[0, :]).astype("int")

        # loop over the (x, y) coordinates and radius of the circles
        for (x, y, r) in circles:
            # draw the circle in the output image, then draw a rectangle
            # corresponding to the center of the circle
            cv2.circle(faceImage, (x, y), r, (0, 0, 255), 1)
    return faceImage, circles
