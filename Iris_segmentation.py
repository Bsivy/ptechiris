import mediapipe as mp
import cv2
import numpy as np
import math
from pathlib import Path
import os
import pandas as pd
# import Corneal_reflect_detection
import New_reflet_detection
import refineIris

# Left Iris indices
LEFT_IRIS = [473, 474, 475, 476, 477]

# Right Iris indices
RIGHT_IRIS = [468, 469, 470, 471, 472]

nom_de_Photo_1 = []
Reflet_OG_1 = []
Reflet_OD_1 = []
Limbe_inferieur_OG_1 = []
Limbe_superieur_OG_1 = []
Limbe_inferieur_OD_1 = []
Limbe_superieur_OD_1 = []
Ratio_OG_1 = []
Ratio_OD_1 = []

nom_de_Photo_2 = []
Reflet_OG_2 = []
Reflet_OD_2 = []
Limbe_inferieur_OG_2 = []
Limbe_superieur_OG_2 = []
Limbe_inferieur_OD_2 = []
Limbe_superieur_OD_2 = []
Ratio_OG_2 = []
Ratio_OD_2 = []

faceMesh = mp.solutions.face_mesh.FaceMesh(refine_landmarks=True)
mpDraw = mp.solutions.drawing_utils
drawSpec = mpDraw.DrawingSpec(thickness=1, circle_radius=2, color=(255, 255, 255))


# landmark detection function
def landmarksDetection(img, results, draw=False):
    img_height, img_width = img.shape[:2]
    # list[(x,y), (x,y)....]
    mesh_coord = [(int(point.x * img_width), int(point.y * img_height)) for point in
                  results.multi_face_landmarks[0].landmark]

    # returning the list of tuples for each landmarks
    return mesh_coord


fileslist = []
for (rep, subrep, files) in os.walk('../Photo/Patient de 1 a 10/Patient 10'):
    fileslist.extend(files)

for file in fileslist:
    filePath = '../Photo/Patient de 1 a 10/Patient 10/' + file
    img = cv2.imread(filePath)
    img = cv2.resize(img, None, fx=0.25, fy=0.25, interpolation=cv2.INTER_CUBIC)
    imgRGB = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    results = faceMesh.process(imgRGB)

    if results.multi_face_landmarks:
        mesh_coords = landmarksDetection(img, results, False)
        right_coords = [mesh_coords[p] for p in RIGHT_IRIS]
        left_coords = [mesh_coords[p] for p in LEFT_IRIS]

        # Left coords
        left_centre_coords = left_coords[0]
        left_point1 = left_coords[1]
        left_point2 = left_coords[2]
        left_point3 = left_coords[3]
        left_point4 = left_coords[4]

        # Right coords
        right_centre_coords = right_coords[0]
        right_point1 = right_coords[1]
        right_point2 = right_coords[2]
        right_point3 = right_coords[3]
        right_point4 = right_coords[4]

        # Left rayon
        left_rayon1 = math.hypot(left_point1[0] - left_centre_coords[0], left_point1[1] - left_centre_coords[1])
        left_rayon2 = math.hypot(left_point2[0] - left_centre_coords[0], left_point2[1] - left_centre_coords[1])
        left_rayon3 = math.hypot(left_point3[0] - left_centre_coords[0], left_point3[1] - left_centre_coords[1])
        left_rayon4 = math.hypot(left_point4[0] - left_centre_coords[0], left_point4[1] - left_centre_coords[1])
        # print(left_rayon1)
        left_rayon = max(left_rayon1, left_rayon2, left_rayon3, left_rayon4)
        # print(left_rayon)

        # Right rayon
        right_rayon1 = math.hypot(right_point1[0] - right_centre_coords[0], right_point1[1] - right_centre_coords[1])
        right_rayon2 = math.hypot(right_point2[0] - right_centre_coords[0], right_point2[1] - right_centre_coords[1])
        right_rayon3 = math.hypot(right_point3[0] - right_centre_coords[0], right_point3[1] - right_centre_coords[1])
        right_rayon4 = math.hypot(right_point4[0] - right_centre_coords[0], right_point4[1] - right_centre_coords[1])
        # print(left_rayon1)
        right_rayon = max(right_rayon1, right_rayon2, right_rayon3, right_rayon4)
        # print(right_rayon)

        # isolate and imwrite circles
        height, width, _ = img.shape
        imeyepoints_right = cv2.threshold(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), 200, 255, cv2.THRESH_BINARY)
        # right_points = [right_coords[1], right_coords[2], right_coords[3], right_coords[4]]

        maskRight = np.zeros((height, width), np.uint8)
        cv2.circle(maskRight, right_centre_coords, int(right_rayon), (255, 255, 255), thickness=-1)
        maskedRight = cv2.bitwise_and(img, img, mask=maskRight)
        cv2.imwrite("../Photo/Results/right_eye_result/" + Path(filePath).stem + "_right_eye.PNG", maskedRight)
        img, circlesRight = refineIris.refineIris(maskedRight, img, Path(filePath).stem + "_rightsegm.PNG")

        # update masks
        maskRight = np.zeros((height, width), np.uint8)
        cv2.circle(maskRight, (circlesRight[0][0], circlesRight[0][1]), circlesRight[0][2], (255, 255, 255), thickness=-1)
        maskedRight = cv2.bitwise_and(img, img, mask=maskRight)

        maskLeft = np.zeros((height, width), np.uint8)
        cv2.circle(maskLeft, left_centre_coords, int(left_rayon), (255, 255, 255), thickness=-1)
        maskedLeft = cv2.bitwise_and(img, img, mask=maskLeft)
        cv2.imwrite("../Photo/Results/left_eye_result/" + Path(filePath).stem + "_left_eye.PNG", maskedLeft)
        img, circlesLeft = refineIris.refineIris(maskedLeft, img, Path(filePath).stem + "_leftsegm.PNG")

        # update masks
        maskLeft = np.zeros((height, width), np.uint8)
        cv2.circle(maskLeft, (circlesLeft[0][0], circlesLeft[0][1]), circlesLeft[0][2], (255, 255, 255), thickness=-1)
        maskedLeft = cv2.bitwise_and(img, img, mask=maskLeft)

        # Corneal reflect
        #  x, y = Corneal_reflect_detection.reflect(maskedRight)
        #  reflectRight = [x, y]
        #  x, y = Corneal_reflect_detection.reflect(maskedLeft)
        #  reflectLeft = [x, y]

        # Nouvelle methode
        x_R, y_R = New_reflet_detection.new_reflet(maskedRight, circlesRight)
        reflectRight = [x_R, y_R]
        for i in range(-2, 3):
            img[reflectRight[0] + i, reflectRight[1]] = [0, 255, 0]
            img[reflectRight[0], reflectRight[1] + i] = [0, 255, 0]

        x_L, y_L = New_reflet_detection.new_reflet(maskedLeft, circlesLeft)
        reflectLeft = [x_L, y_L]
        # print(Path(filePath).stem, reflectLeft)
        for i in range(-2, 3):
            img[reflectLeft[0] + i, reflectLeft[1]] = [0, 255, 0]
            img[reflectLeft[0], reflectLeft[1] + i] = [0, 255, 0]

        # Draw le circle of iris
        # cv2.circle(img, left_centre_coords, int(left_rayon), (0, 255, 0), 1, cv2.LINE_AA)
        # cv2.circle(img, right_centre_coords, int(right_rayon), (0, 255, 0), 1, cv2.LINE_AA)

        # cv2.circle(img, left_centre_coords, radius=0, color=(0, 0, 255), thickness=1)
        # cv2.circle(img, right_centre_coords, radius=0, color=(0, 0, 255), thickness=1)
        # cv2.circle(img, right_point1, radius=0, color=(0, 0, 255), thickness=1)
        # cv2.circle(img, right_point2, radius=0, color=(0, 0, 255), thickness=1)
        # cv2.circle(img, right_point3, radius=0, color=(0, 0, 255), thickness=1)
        # cv2.circle(img, right_point4, radius=0, color=(0, 0, 255), thickness=1)
        # cv2.circle(img, left_point1, radius=0, color=(0, 0, 255), thickness=1)
        # cv2.circle(img, left_point2, radius=0, color=(0, 0, 255), thickness=1)
        # cv2.circle(img, left_point3, radius=0, color=(0, 0, 255), thickness=1)
        # cv2.circle(img, left_point4, radius=0, color=(0, 0, 255), thickness=1)

        # Calculate the ratio
        tmp_Limbe_inferieur_OG = circlesLeft[0][1] + circlesLeft[0][2]
        tmp_Limbe_superieur_OG = circlesLeft[0][1] - circlesLeft[0][2]
        tmp_Limbe_inferieur_OD = circlesRight[0][1] + circlesRight[0][2]
        tmp_Limbe_superieur_OD = circlesRight[0][1] - circlesRight[0][2]
        ratio_OG = (reflectLeft[0] - tmp_Limbe_superieur_OG) / (tmp_Limbe_inferieur_OG - tmp_Limbe_superieur_OG)
        ratio_OD = (reflectRight[0] - tmp_Limbe_superieur_OD) / (tmp_Limbe_inferieur_OD - tmp_Limbe_superieur_OD)

        if file.endswith('1.JPG'):
            nom_de_Photo_1.append(Path(filePath).stem)
            Reflet_OG_1.append(reflectLeft[0])
            Reflet_OD_1.append(reflectRight[0])
            Limbe_inferieur_OG_1.append(tmp_Limbe_inferieur_OG)
            Limbe_superieur_OG_1.append(tmp_Limbe_superieur_OG)
            Limbe_inferieur_OD_1.append(tmp_Limbe_inferieur_OD)
            Limbe_superieur_OD_1.append(tmp_Limbe_superieur_OD)
            Ratio_OG_1.append(ratio_OG)
            Ratio_OD_1.append(ratio_OD)

            dict = {'Nom_de_Photo': nom_de_Photo_1,
                    'Limbe_inferieur_OD': Limbe_inferieur_OD_1, 'Reflet_OD': Reflet_OD_1,
                    'Limbe_superieur_OD': Limbe_superieur_OD_1,
                    'Limbe_inferieur_OG': Limbe_inferieur_OG_1, 'Reflet_OG': Reflet_OG_1,
                    'Limbe_superieur_OG': Limbe_superieur_OG_1,
                    'Ratio_OG': Ratio_OG_1, 'Ratio_OD': Ratio_OD_1}
            df = pd.DataFrame(dict)
            df.to_csv('../Data_Resultat/Patient de 1 a 10/Patient 10_1.csv', index=False)
        else:
            nom_de_Photo_2.append(Path(filePath).stem)
            Reflet_OG_2.append(reflectLeft[0])
            Reflet_OD_2.append(reflectRight[0])
            Limbe_inferieur_OG_2.append(tmp_Limbe_inferieur_OG)
            Limbe_superieur_OG_2.append(tmp_Limbe_superieur_OG)
            Limbe_inferieur_OD_2.append(tmp_Limbe_inferieur_OD)
            Limbe_superieur_OD_2.append(tmp_Limbe_superieur_OD)
            Ratio_OG_2.append(ratio_OG)
            Ratio_OD_2.append(ratio_OD)

            dict = {'Nom_de_Photo': nom_de_Photo_2,
                    'Limbe_inferieur_OD': Limbe_inferieur_OD_2, 'Reflet_OD': Reflet_OD_2,
                    'Limbe_superieur_OD': Limbe_superieur_OD_2,
                    'Limbe_inferieur_OG': Limbe_inferieur_OG_2, 'Reflet_OG': Reflet_OG_2,
                    'Limbe_superieur_OG': Limbe_superieur_OG_2,
                    'Ratio_OG': Ratio_OG_2, 'Ratio_OD': Ratio_OD_2}
            df = pd.DataFrame(dict)
            df.to_csv('../Data_Resultat/Patient de 1 a 10/Patient 10_2.csv', index=False)

    # Writing image with circle and iris landmarks
    cv2.imwrite("../Photo/Results/face_results/" + Path(filePath).stem + "_full_face.PNG", img)

    # cv2.imshow("img", img)
    # cv2.waitKey(0)
