import numpy as np
import cv2
import statistics


def reflect(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)
    _, im_bw = cv2.threshold(gray, 254, 255, cv2.THRESH_BINARY)

    # midle of the shape
    mass_x, mass_y = np.where(im_bw >= 255)
    cent_x = int(statistics.median(mass_x))
    cent_y = int(statistics.median(mass_y))
    for i in range(-2, 3):
        img[cent_x + i, cent_y] = [0, 255, 0]
        img[cent_x, cent_y + i] = [0, 255, 0]
    return cent_x, cent_y
